/*
 * SerialICE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

// SuperIO registers
#define NSC_CLOCKCF 0x29
#define NSC_CLOCKCF_CKEN (1<<7)
#define NSC_CLOCKCF_CKOUTSEL (1<<6)
#define NSC_CLOCKCF_CK48SEL (1<<5)
#define NSC_CLOCKCF_CKVALID (1<<4)
#define NSC_CLOCKCF_LOCKCCF (1<<3)

#define NSC_SP1CF      0xF0  // Serial Port 1 Configuration register
#define NSC_SP1CF_BSE (1<<7) // Bank Select Enable
#define NSC_SP1CF_BI  (1<<2) // Busy Indicator
#define NSC_SP1CF_PMC (1<<1) // Power Mode Control
#define NSC_SP1CF_TSC (1<<0) // TRI-STATE Control

// PC87108 bank selection
#define PC87108_BSR     0x03 // Bank Select Register
#define PC87108_BSR_BK0 0x00 // Bank 0
#define PC87108_BSR_BK1 0x80
#define PC87108_BSR_BK2 0xE0
#define PC87108_BSR_BK3 0xE4
#define PC87108_BSR_BK4 0xE8
#define PC87108_BSR_BK5 0xEC
#define PC87108_BSR_BK6 0xF0
#define PC87108_BSR_BK7 0xF4

// PC87108 bank 2
#define PC87108_BGDL 0x00 // BGD(L)
#define PC87108_BGDH 0x01 // BGD(H)

#define PC87108_EXCR2                0x04 // Extended Control Register 2
#define PC87108_EXCR2_TF_SIZ0 (1<<0) // TX_FIFO Levels Select
#define PC87108_EXCR2_TF_SIZ1 (1<<1)
#define PC87108_EXCR2_RF_SIZ0 (1<<2) // RX_FIFO Levels Select
#define PC87108_EXCR2_RF_SIZ1 (1<<3)
#define PC87108_EXCR2_PRESL0  (1<<4) // Prescaler Select
#define PC87108_EXCR2_PRESL1  (1<<5)
#define PC87108_EXCR2_LOCK    (1<<7)

#define PC87108_EXCR2_PRESL_13_0   0
#define PC87108_EXCR2_PRESL_1_625  PC87108_EXCR2_PRESL0
#define PC87108_EXCR2_PRESL_1_0    ( PC87108_EXCR2_PRESL1 | PC87108_EXCR2_PRESL0 )

/* Baudrate table using CONFIG_SERIAL_BAUDRATE */
#ifdef __ROMCC__
	#define NSC_BAUD(baud) NSC_BAUD_ ## baud
#else
	#define _NSC_CAT(a, b) a ## b
	#define NSC_BAUD(baud) _NSC_CAT(NSC_BAUD_, baud)
#endif
/* pass divisor as a single number, like in the datasheet */
#define NSC_BGDH_BGDL(div) (div >> 8), (div & 0xFF)
/* PRESL, BGD(H), BGD(L) */
/* Every baudrate above 115200 has BGD(H) equal to 0 */
#define NSC_BAUD_230400    PC87108_EXCR2_PRESL_1_625, NSC_BGDH_BGDL(4)
#define NSC_BAUD_460800    PC87108_EXCR2_PRESL_1_625, NSC_BGDH_BGDL(2)
#define NSC_BAUD_921600    PC87108_EXCR2_PRESL_1_625, NSC_BGDH_BGDL(1)
#define NSC_BAUD_1500000   PC87108_EXCR2_PRESL_1_0,   NSC_BGDH_BGDL(1)

static inline void udelay(unsigned usecs)
{
	int i;
	for(i = 0; i < usecs; i++)
		inb(0x80);
}

static inline u8 pnp_read_register(u16 port, u8 reg)
{
	outb(reg, port);
	return inb(port +1);
}

static inline int nsc_poll_clk_stable(u16 sio_port, int timeout)
{
	while (!(pnp_read_register(sio_port, NSC_CLOCKCF) & NSC_CLOCKCF_CKVALID) && timeout--)
		udelay(1000);
	if (!timeout)
		return 1;

	return 0;
}

static inline void pc87108_set_baudrate(u16 base_port, u8 presl, u8 bgdh, u8 bgdl){
	// Switch to bank 2 of serial port
	outb(PC87108_BSR_BK2, base_port + PC87108_BSR);
	// Set divisor
	outb(bgdh, base_port + PC87108_BGDH);
	outb(bgdl, base_port + PC87108_BGDL);
	// Set prescaler and lock
	outb(presl | PC87108_EXCR2_LOCK, base_port + PC87108_EXCR2);
	// Restore BSR to default Bank 0. This will also clear all bits in LCR.
	outb(0, base_port + PC87108_BSR);
}

static void superio_init(u16 sio_port)
{
	int timeout = 100000;
	/* Use 14.318MHz CLK from CLKIN pin */
	pnp_write_register(sio_port, NSC_CLOCKCF,
	NSC_CLOCKCF_CK48SEL | NSC_CLOCKCF_CKEN);
	// TODO: POST CODE for failed clock
	nsc_poll_clk_stable(sio_port, 100);
	// Enable SP1/COM1
	pnp_set_logical_device(sio_port, 3);
	pnp_set_enable(sio_port, 1);
	// Enable banking on SP1
	pnp_write_register(sio_port, NSC_SP1CF, NSC_SP1CF_PMC | NSC_SP1CF_BSE);

	#if CONFIG_SERIAL_BAUDRATE > 115200
		#if CONFIG_SERIAL_BAUDRATE == 230400 || \
			CONFIG_SERIAL_BAUDRATE == 460800 || \
			CONFIG_SERIAL_BAUDRATE == 921600 || \
			CONFIG_SERIAL_BAUDRATE == 1500000
			#warning Using high baudrate for UART
			// Set baudrate and lock
			pc87108_set_baudrate(CONFIG_SERIAL_PORT,
				NSC_BAUD(CONFIG_SERIAL_BAUDRATE));
		#else
			#error Error: CONFIG_SERIAL_BAUDRATE set to unsupported baudrate
		#endif
	#endif

}

static int superio_serial_divisor(int baudrate)
{
	/* Divisor cannot be affected after setting PC87108_EXCR2_LOCK.
	 * Return divisor value for 115200 just in case.
	 */
	return 1;
}
