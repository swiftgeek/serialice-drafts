/*
 * SerialICE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

/*
 * Lenovo Thinkpad X200 (Wistron Mocha-1), devel BOM only!
 * Requires WPCN385 SIO and UART connector to be populated,
 * which are not present on production units.
 */

/* Following string cannot exceed 32-chars,
 * and needs to match lua script filename (without extension).
 */
const char boardname[33]="Lenovo X200                     ";

static void chipset_init(void)
{
	southbridge_init();
	/* Enable access to SuperIO on ICH9M - GEN1_DEC */
	/* range 0x1600 - 0x167f */
	pci_write_config32(PCI_ADDR(0, 0x1f, 0, 0x84), 0x007c1601);
	/* Enable EC access as well - MC_LPC_EN */
	pci_write_config16(PCI_ADDR(0, 0x1f, 0, 0x82), 0x3c0b);

	superio_init(0x164E);
}
